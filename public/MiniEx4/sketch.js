// random numbers, words and button declared
var num = 0;
let likes = ["likes", "dislikes"];
let button;
var comment = ["happy now?", "feel good?", "feel better?", "accurate?","feel confident?", "pleased?","👍","👎"];

// load pictures
function preload() {
  img1 = loadImage('like.png');
  img2 = loadImage('dislikes.png');
}

function setup() {
createCanvas(600, 400);
background(220);

// arrow
noStroke();
fill(59,89,152);
  triangle(307, 65, 292, 80, 322, 80);
  rect(302,80,10,20);

// 'rate me' button
button = createButton('rate me');
button.position(280, 40);
button.mousePressed(pickNumber);
}

function draw() {
console.log(mouseX,mouseY)
}

function pickNumber() {
// puts new background when pressed, so new number can appear
background(220);
fill(255);
stroke(59,89,152);
strokeWeight(5);
  rect(110,20,400,300);
frameRate(1);

// random picked comments for user
var print = comment[Math.floor(Math.random()*comment.length)];
fill(200);
textSize(20);
stroke(59,89,152);
strokeWeight(5);
  text(print, random(600), random(400));

// random number generator between 1-1000 w. no decimals bcs of "floor"
num = floor(random(0, 10001));
fill(59,89,152);
noStroke();
textSize(35);
  text(num, 220, 120);

// random pick between likes and dislikes
let rate = likes[Math.floor(Math.random()*likes.length)];
textSize(35);
  text(rate, 310, 120);

// "your rating:"
fill(0);
textSize(15);
noStroke();
  text('Your rating:', 150, 80);

}
