## R8 ur w0rth

### Link to my program:
[MiniEx4](https://MiraMorso.gitlab.io/ap-2020/MiniEx4/)

### Link to repository:
https://gitlab.com/MiraMorso/ap-2020/-/tree/master/public/MiniEx4

### Screenshot:
![ScreenShot](rateme.png)


### Questions and Answers:

***Provide a title of your work and a short description of the work (within 1000 characters) as if you are submitting to the festival open call.***
The title of my work this week is “R8 ur w0rth”. What’s your worth exactly if you put it down in numbers? What if this number was a number of likes or dislikes? Do other people’s perception of you determine your worth? With this work I want to show the ironic side of, how the So Me society is affecting every single person, and how they look at themselves. In our lives ALL is CAPTUREd down to the specifics and numbers. We are defined by the number of likes and the amount of attention we receive from random people talking through screens, but do high numbers and ratings really make us feel happy and accomplished or is it just a way to show of a picture-perfect appearance. 


***Describe your program and what you have used and learnt.***
With my program I wanted to try out buttons, and make a more interactive program than the last few times. I started looking at some different examples made by other people. I looked at a sketch by Sumptous Principal, and liked the old school simple look of their button. The overall idea I had at this point was to create a rating system, that would tell the user what amount of either likes or dislikes they “are worth”. To do this I had to find out, how to make random numbers and a random pick between variables. I again started searching for example since  I had no idea, and couldn’t seem to find usable information in the p5.js reference library. I read a discussion on stackoverflow.com and suddenly I had numbers with ten decimales changing every second. Since likes can’t possibly come in decimals, this was also an issue.
I finally figured out how to make these numbers have a connection with my button by creating my own function called pickNumber( ), and then through p5.js I learned that using floor(random(0,10001)); I could make the numbers 1-10.000 get picked at random without any decimals. 
To make the program choose between different variables of text I looked through https://happycoding.io/tutorials/p5js/random#text-generation . To give more of an obvious purpose, I made small comments for the user appear at random around the canvas with every push. To make the user able to push multiple times, I added background to the function. To make the user wanna push the button I also created an arrow of a triangle and a rectangle at the beginning, which after now press will disappear behind a rect functioning as a form of window or post that we know from social media. 
I really wanted to have pictures in pngs pop up according to the words that was picked in my likes/dislikes array. But after many different tries, I ran out of time. That is why two pictures are preloaded in the beginning of the code. The closest I could get to a like and dislike icon, was emojis in my array of comments. 

One of my tries at matching a picture of a like button to randomly picked text from array:

`if (likes[Math.floor(Math.random()*likes.length)].includes('likes') == true){


  image(img1,30,30,40,40)
}
 
 else {
   image(img2,50,50,40,40)
 }`



***Articulate how your program and thinking address the theme 'capture all’. What are the cultural implicatons of data capture?***
Like I said in my description data is being captured of everything about us and around us, and put down in descriptions, numbers, statistics etc. Likes and dislikes is another form of data that we often try to capture to maintain the facade we want the people looking at us to actually see more than our actual self. When we post something on social media data is being captured about, how other people are interacting with the post. And this form of data is in this So Me society seen as high value. 



***References:***

*  button changing background - p5.js editor by Sumptous Principal

*  https://stackoverflow.com/questions/48038491/generate-a-random-integer-in-p5-js

*  https://happycoding.io/tutorials/p5js/random#text-generation

***Facebook blue:***
(Color code to create a familiar feeling from the well-known Facebook media)
*  http://www.perbang.dk/rgb/3B5998/



