## huMANerrOR

*Made by Emma Marie Kongsbak Bertelsen & Mira Bella Dyring Morsø*

### Link to program:
[MiniEx8 RUNME](https://MiraMorso.gitlab.io/ap-2020/MiniEx8/)

### Link to repository:
https://gitlab.com/MiraMorso/ap-2020/-/tree/master/public/MiniEx8

### Screenshot:
![ScreenShot](screenshot.png)

### Title and conceptual description:

The title of our program is: “huMANerrOR”
With our sketch we want to comment on bullying, and how it has only become worse because of the access to internet that has highly increased since the origin of the World Wide Web. Also under the name: Cyber Bullying. With our program, we comment on how horrible bullying is - especially online where the bullies can hide behind their screens. Additionally, we comment on how AI are becoming a bigger and bigger part of our daily lives as to why AI’s might have a mind of their own in the future thinking of for instance Alexa, Siri and the Google Assistant - They do not have a mind of their own, however they are programmed to response to personal questions such as “Hey Alexa, what is your love-life like?” as well as questions with an exact answer such as “Hey Alexa, how is the weather outside?”. Thinking of how this could evolve, we would like to think that AIs (and the internet) would not be okay with cyber bullying (among others). On that note, our program is called huMANerrOR because of how humans are the ones doing the harm along with the fact that the computer can not execute because of how it does not understand the need to bully and harm other people, as to why the it describes the error to the user being: “human error” in order to make the user understand the error. The reason why our program is called “huMANerrOR” and not “human error” is up for interpretation.  

### Technical description:

Our program is mostly visual and not very interactive. We decided to focus on the poetic message that the sketch is trying to give. 
We used a bunch of rect figures to create a big pixelated file with x’es as eyes, similar to the once you receive, when your website is unable to refresh. Within the figure is text that other than saying “ERROR” multiple places also says a bunch of statements evolving around the theme of bullying, which still is a big problem in the world we live it. All of these statements and messages are written down in a file called “error.json,” which we draw them from via our sketch file. Other than that we have used a few if statements to change the color of the text, and to activate a voice message saying: “ERROR.” These if statements are activated with both the position of the mouse but also whether or not the mouse is pressed. 
Additional note: Should one wish to revisit our program later to rewrite the code, one could consider to write the code likewise to the Queer-example from class 9. 

### Our process:

1. We started out by figuring out what out concept would be [being the internet’s reaction (crashing) to cyberbullying if the internet had a personality and a mind of its own. This hopefully signalizes several things such as: the fact that bullying is not okay, that the internet doesn’t have to be a bad thing and that AI is becoming a bigger and bigger part of our daily lives]. 
2. Then we tried to make the teletype package in atom work - it demanded a little more effort than the above, but we figured it out at last. 
3. Next up was figuring out what we were going to program as to why we tried to show each other what we think of when the internet crashes. We decided to combine both of our suggestions which meant trying to figure out the best way to do this.
4. Our first idea was to make the “face” of the internet with text - this was not going to work because of the minimal amount of text and the positions of the text. We then decided to draw the outline of the “face” with grey rectangles like the rectangles in the picture and fill the face with our statements instead. 
5. After we had drawn the face in draw we discussed the possibility of making the face into a class which we tried to do of course, however we soon realized that it was a mistake and that our original plan with drawing it in the draw function() was better for us in this context. 
6. Next, we had to figure out how to make our .json file. This turned out to be a struggle. After a lot of tries and use of the json validator, we come to find out that a comma, was what made or sketch not work. And we have now made a successful connection between our sketch and our json file. At this point all we had to do was fill our “face” with the statements and sentences in our json file. Which made the code very long since almost every statement needed its own size and placement to fit. 
7. After this we felt like the sketch was missing something and we decided to make the text change its fill depending on the x value of the mouse, but this wasn’t quite enough to make the result satisfying to us. 
8. We decided to create a mp3 of a google translator say “error”, and make it play with every press of the mouse. This is to make “the computer” stop the user from going any further [into cyber bullying], there is an error that is impossible to get away from. No matter where you press you’ll get the same message: ERROR!


### Analysis: 

In terms of the analysis of our program huMANerrOR we focus on the text The Aesthetics of Generative Code. The text mainly focus on senses and how we can only “appreciate generative code fully” if we “‘sense’ the code” so that we can “fully grasp what it is we are experiencing and to build an understanding of the code’s actions.”. When thinking of what it means to sense the code, we seek to describe the senses that the user uses when running our program; The senses are: The sense of touch (when the mouse is pressed and when the mouse is moved), the sense of sight (the “face” and the text written as the skin of the “face” along with the change of fill() when the mouse is moved) and the sense of hearing (the google translate voice saying “error”). Not all senses are used, however, we still find that our use of senses makes our program better and thereby allows the user to understand our program better. 
When thinking of poetry, the text states that: “poetry can neither be reduced to audible signs (the time of the ear) nor visible signs (the space of the eye) but is composed of language itself. This synthesis suggests that written and spoken forms work together to form a language that we appreciate as poetry. “. We would like to believe that thus our program is not a poem, it can be seen as poetic due to how our program expresses “emotions” (statements) through “art” and senses. Additionally, we believe that our program can be interpreted differently depending on the user’s experience with the program - we do, however, believe that the user receives the message because of our combination of written and spoken words. 

### References: 
Text used in analysis
http://www.generativeart.com/on/cic/2000/ADEWARD.HTM 

Face of the internet - inspiration
https://www.google.com/url?sa=i&url=https%3A%2F%2Fmedium.com%2F%40martatatiana%2Fpoor-detective-angular2-browser-crash-and-debugelement-f0a651dbf33&psig=AOvVaw3xl_f0hTcLwsKE7Rs4BUN2&ust=1585573420343000&source=images&cd=vfe&ved=0CAIQjRxqFwoTCLCTlbjfv-gCFQAAAAAdAAAAABAD


Trying to understand json with this example:
https://editor.p5js.org/kimagosto/sketches/ryr40qHJ4

Finding statements for the text:
https://onlinesense.org/bullying-quotes/