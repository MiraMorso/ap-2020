### Link to my program:
[MiniEx2](https://MiraMorso.gitlab.io/ap-2020/MiniEx2/)

### Link to repository: 
https://gitlab.com/MiraMorso/ap-2020/-/tree/master/public/MiniEx2

### Screenshot:
![ScreenShot](MiniEx2screenshot.png)


### Questions and Answers
**Describe your program and what you have used and learnt.**

I sat and looked through the existing Emojis on the Apple keyboard trying to figure out, what haven't already been made. I noticed that different jobs, activities and characteristics were all portrayed in man, woman and none-binary, except from the bride and groom emojis. The only wedding emojis available, are a man in a tuxedo and a woman in a white dress and vail. I was surprised by this, but found it a good opportunity for this second task. I then found pictures of both the man in tuxedo and woman emojis, and thought about how I could recreate a mix of both for my emoji. I used a lot of ellipses, since those are very simple to manipulate with. But later got to find out that the hair and eyes weren’t really working for me. I tried arc for the hair, which after a lot of altering turned out pretty alright. I created the tuxedo and body with vertex and a square with a fourth variable to create the form of the shoulders. After being done with the body of the emoji, I was still unhappy with the eyes, as they were at the edge of being creepy, so I decided to look for alternatives, and found a bezierVertex in a program on Google that resembled an actual eye more than just an ellipse. And decided to play around with the variables to understand, how it worked. This took a while. But I finally managed to make the figure both smaller and understand how to change the coordinates. I added it into my program, and messed the whole emoji up. It seemed that the function “translate” was causing all figures under the bezierVertex to move with it. Later I learned that you need to ass “push();” and “pull();” in the code above and under, what you want to be affected by “translate,” and then the emoji was back to normal and finished.
I also used  a RGB Color Chart to pick the exact colors I wanted, but at this point I feel like I’ve gotten somewhat of an understanding of how to alter the colors myself too. 

For the second emoji, I wanted something associated with climate change, and looked for symbols for climate change on Google. But here it only seemed to be green and mostly positive portrayals. I decided I wanted something that could also be more like a warning sign for the changes on earth, here focussing on the rise in temperatures. I thought for a long time about how to create the earth, but then decided I wanted to know how to insert and code with pictures. I found a picture in red colors of earth, saved it, and put it in my MiniEx2 folder for easy access. And I watched a video from the Coding Train on YouTube, and eventually after a lot of attempts, I managed to add a picture to my program. The colors of the picture didn’t really give me the look I wanted, so I added an ellipse on top, put fill as orange and then added a variable to put down the opacity, which I learned though reference on the p5.js website. I then coded another ellipse with a low height under the globe, again with a fourth variable for opacity, to work as a shadow, to make it feel more like an emoji. But after that I still felt that it needed more to fit in the existing emojis. I decided to add a drop of sweat as seen on a few of Apple’s emojis in the top left corner to indicate sweating. I made this of Triangles and ellipses in different sizes to it gave more a feel of dimension. 

Throughout the whole thing I had “console.log(mouseX,mouseY)” running, to make it easier to find the right coordinates through console in the browser preview. I found this very helpful.
 

**How would you put your emoji into a wider cultural context that concerns representations, identity, race, social, economics, culture, device politics and beyond?**

The right to marry whoever you want to and however you want to has been an important area for many many years. And in some parts of the world is same-sex marriages not acceptable, which is crazy when you think about the fact that we live in 2020. I was very surprised to see only the generic bride and groom emojis were available, since Apple has so much focus on including everyone no matter how they identify themselves, so why not a woman in a tuxedo and a man in a dress? So as an attempt to include more people, I wanted to create the woman in a tuxedo.
 But with this I also decided to go with the colors that Apple themselves use as the default skin color: Yellow. I felt that making the emoji a specific realistic skin color would maybe sent a message that wasn’t intended. Which also made me think why the yellow is a default color. To me as including as it gets, I should have made it an option to change the skin color to a range of different colors. But since I focused more on how people identify and choose to appear to the world, this wasn’t a part of the progress.

The second emoji is an attempt to shed more light on the important and very dangerous consequences of global warming and climate change, which is a serious problem that the whole world is facing. I feel that this is absolutely necessary, since we seem to look away from the reality that is the situation for all of us. People don’t talk or think about this enough, and therefore I think that emojis that are really just an everyday way of communication needs to be more serious and also show the negative side of things for it to be a part of our everyday. 




### References
RBG Color Chard: https://www.rapidtables.com/web/color/RGB_Color.html

7.8 Objects and Images - p5.js Tutorial: https://www.youtube.com/watch?v=i2C1hrJMwz0&t=473s

bezierVertex in WebEditor: https://editor.p5js.org/jackiehu/sketches/Sy7JkKJk4
