
let img;

function setup() {
  createCanvas(500, 300);
  img = loadImage('1.png');
}

function draw() {
  background(188,226,245);
//1st emoji - Woman in tuxedo

//hair behind
fill(243,184,22);
noStroke();
ellipse(100, 101, 120, 150);
triangle(100,165,159,115,160,160)
triangle(40,160,41,115,120,165)

// body
fill(0)
square(40, 167, 121, 20);

fill(245)
beginShape();
vertex(85, 170);
vertex(120, 170);
vertex(120, 290);
vertex(85, 290);
endShape();


fill(170)
beginShape();
vertex(102, 175);
vertex(107, 175);
vertex(107, 290);
vertex(102, 290);
endShape();

//head and ears
noStroke();
fill(255, 204, 0);
ellipse(100, 100, 100, 100);
ellipse(52, 100, 20, 30);
ellipse(148, 100, 20, 30);
rect(90,125,25,47 )

//eyebrows
fill(243,174,52);
ellipse(78, 87, 25, 20);
ellipse(120, 87, 25, 20);

fill(255, 204, 0);
ellipse(80, 92, 30, 20);
ellipse(118, 92, 30, 20);

//mouth
fill(255)
ellipse(100, 129, 30, 17);
fill(255, 204, 0);
ellipse(100, 125, 30, 10);

//nose
fill(243,184,22);
ellipse(100, 115, 15, 10);
fill(255, 204, 0);
ellipse(100, 111, 15, 10);

//eyes
push();
fill(255);
stroke(0);
strokeWeight(0.5);
translate(80,95);
beginShape();
vertex(-15,0);
bezierVertex(-2,-12,2,-12,15,0);
bezierVertex(2,12,-2,12,-15,0)
endShape();

translate(40,1);
beginShape();
vertex(-15,0);
bezierVertex(-2,-12,2,-12,15,0);
bezierVertex(2,12,-2,12,-15,0)
endShape();
pop();

//pupils
fill(0);
ellipse(84, 97, 15, 13);
ellipse(124, 97, 15, 13);
fill(255)
ellipse(84, 93, 5, 5);
ellipse(124, 93, 5, 5);

//hair
fill(243,184,22);
  arc(150, 60, 80, 60, radians(72), radians(225));
arc(65, 40, 100, 90, radians(370), radians(120));

//clothes
fill(255);
stroke(0);
strokeWeight(0.7);
beginShape();
vertex(140, 165);
vertex(115, 184);
vertex(104, 168);
vertex(120, 150);
endShape();

beginShape();
vertex(65, 165);
vertex(84, 150);
vertex(105, 168);
vertex(96, 184);
endShape();

//bowtie
fill(0)
triangle(105, 175, 80, 160, 80, 183);
triangle(125, 183, 100, 175, 125, 160);

fill(188,226,245);
noStroke();
square(40,255,140)

//2nd emoji - Overheating Globe
//globe image and shadow
fill(40, 40, 40, 50)
ellipse(335, 230, 120, 20);
image(img,190,60,290,170);

//orange sheer ellipse
fill(255, 110, 0, 100)
ellipse(335, 145, 147, 150);

//sweatdrop
fill(21, 107, 192);
noStroke();
triangle(275, 110, 290, 70, 305, 110);
ellipse(290, 115, 30, 30);

fill(102, 204, 255);
noStroke();
triangle(290, 81, 280, 110, 300, 110);
ellipse(290, 115, 22, 22);

fill(255)
triangle(287, 115, 291, 100, 297, 115);
ellipse(292,115,10,10)


console.log(mouseX,mouseY)
}
