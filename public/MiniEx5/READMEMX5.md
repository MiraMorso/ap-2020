## R8 ur w0rth 2.0

### Link to my program:
[MiniEx5](https://MiraMorso.gitlab.io/ap-2020/MiniEx5/)

### Link to repository:
https://gitlab.com/MiraMorso/ap-2020/-/tree/master/public/MiniEx5

### Screenshot:
![ScreenShot](rateme2.0.png)


### Questions and Answers:
***what is the concept of your work? what is the departure point? what do you want to express?***

The concept of this program is to comment on the social media culture that we are living in at this time. It is supposed to portray the Like Economy that Carolin Gerlitz and Anne Helmond talks about in "The Like Economy: Social Buttons and the Data-Intensive Web”.

I liked how in the text they say the following about the like button as it was just released on Facebook: “Like button were introduced in 2009 and were presented as a shortcut to commenting in order to replace short affective statements like ‘Awesome’ and ‘Congrats!’ (Pearlman, 2009)” (p. 8 ) in contrast to how it has evolved into being numbers that helps identify us as people and our worth and even a form of currency gaining more and more value. 

So I wanted to make something that shows how far the whole concept of likes and dislikes have come in our lives. It has become this controlling aspect in our lives that is hard to run away from, since it really is what the whole social media thing is about. We look at something or someone and determine the worth of this (OR THEM) highly based on the amount of feedback often shown as likes or dislikes. 

***what have you changed and why?***

I wanted to further work on my last miniEx, because I didn’t feel like it was personal enough. I showed it to my parents, and my mom didn’t understand the concept or meaning of what she was looking at. Therefore, I had to make the idea behind it more obvious and personal. 

I first added the web cam video to involve the user in front of the computer, and the word “SCANNING” appearing under it, to create the feeling of being observed and measured. Other than that I also wanted to make the whole concept more similar to the well-known social media platform Facebook by adding the Facebook logo, and also create a similar layout to the homepage of Facebook.

***Reflect upon what Aesthetic Programming might be (Based on the readings that you have done before (especially this week on Aesthetic Programming as well as the class01 on Why Program? and Coding Literacy), What does it mean by programming as a practice, or even as a method for design? Can you draw and link some of the concepts in the text and expand with your critical take? What is the relation between programming and digital culture?)***

The digital culture we live in consists of programming and coding. As the world becomes more and more technological, we become more intertwined with programming. Monfort talks about how people "with understanding of programming will gain better perspective on cultural systems that use computation”. And as said the number of cultural systems with computation is increasing fast. That is also why programming is a highly important method in design. To help and make the world we live in better by designing and re-designing we need to understand how things work and how to change them for the better.

Vee’s text though explains how programming and coding is becoming the future reading and writing and describes it as a literacy. This makes sense to me in the way that just as learning to read and write we can learn to read and write programming and code. It is all about just learning and trying it out like learning the alphabet in preschool.




***References:***
*  https://creative-coding.decontextualize.com/text-and-type/
*  "How to Fade in And Out" in Web Editor by remarkability "editor.p5js.org/remarkability/sketches/etM08miUD"

***Facebook blue:***
(Facebook blue)
*  http://www.perbang.dk/rgb/3B5998/




