var num = 0;
let likes = ["likes", "dislikes"];
let button;
var comment = ["happy now?", "feel good?", "feel better?", "accurate?","feel confident?", "pleased?","👍","👎"];


// load pictures
function preload() {
  img3 = loadImage('facebooklogo.png')
  img4 = loadImage('likeandcomment.png')
}

function setup() {
//setup camera capture
let cam = createCapture(VIDEO);
cam.size(580,400);
cam.position(10, 100);

createCanvas(880, 650);
background(225);

// 'rate me' button
button = createButton('rate me');
button.position(670, 150);
button.mousePressed(pickNumber);

// fading for scanning text
fade = 0

// facebook icon
noStroke();
fill(59,89,152);
rect(20,10,850,80,5);
image(img3,10,10,80,80);
fill(255);
rect(580,95,250,410,3);
rect(25,95,550,410,3);
}

function draw() {
// scanning text fading in and out
fill(128 + sin(frameCount*0.1) * 128);
textSize(20);
noStroke();
text('**SCANNING**', 430, 527);
}

function pickNumber() {
// puts new background when pressed, so new number can appear
background(225);
fill(255);
rect(580,95,250,410,3);
rect(25,95,550,410,3);
frameRate(1);

// facebook icon to keep the logo showing
noStroke();
fill(59,89,152);
rect(20,10,850,80,5);
image(img3,10,10,80,80);

//like, comment & share menu
image(img4,23,510,553,75);

// random picked comments for user
var print = comment[Math.floor(Math.random()*comment.length)];
fill(200);
textSize(20);
stroke(59,89,152);
strokeWeight(5);
text(print, random(600), random(400));

// random number generator between 1-1000 w. no decimals bcs of "floor"
num = floor(random(0, 10001));
fill(59,89,152);
noStroke();
textSize(25);
text(num, 645, 250);

// random pick between likes and dislikes
let rate = likes[Math.floor(Math.random()*likes.length)];
textSize(25);
text(rate, 710, 250);

// "your rating:"
fill(0);
textSize(15);
noStroke();
text('Your rating:', 600, 210);
}
