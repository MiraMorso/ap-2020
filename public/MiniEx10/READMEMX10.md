## MiniEx10 - Brainstorming Ideas and Flowcharts 

### Flowcharts for my old MiniEx:

I picked my MiniEx6, which is my beer pong game. I struggeled a bit with figuring out how technical I should be in my flowchart, and ended up not being technical at all. I reckon my mom and dad would be able to understand it. 

![ScreenShot](mygameFlowchart.jpg)


### Questions to think about for the README:
#### What are the challenges between simplicity at the level of communication and complexity at the level of algorithmic procedure?

Some things are easier to express when using the correct term, for example array, preload, setup, draw and so forth, yet people who are unfamiliar with the language will not understand the words. It takes some time, to convert your idea into common tongue, so that it can be a proper communication device, that allows a diverse team of people to corporate. 
At another level of complexity is also heavily influenced by the need for the level of abstraction. How step by step do you need your program to be? Is the process of every piece of code or a more general idea of how the program work you need? When sitting as a studygroup who all has an average understanding of syntaxes, it is more convenient to incorporate the steps on code-like level, to get an idea of how to program is supposed to run. It is not completely step-by-step, but it is as detailed as a first draft of a superficial idea, can/needs to be. 

#### What are the technical challenges for the two ideas and how are you going to address them?

*For E.T (Emotional Translator)*

The overall idea for this, is to make a look a like of the translate feature, we have on Facebook. On Facebook you have the option to get comments and posts translated into your own language, if the text is in a foreign language to you. It gives the user the ability to understand all content shared, even if you don’t understand the language it’s written in. We wanted to play with the idea of the facade we put on, when sharing our lives on social media, and create an “Emotional Translator,” that will give the user the option to understand what the author of the post or comment is really thinking or feeling. 

![ScreenShot](ETflowchart.jpg)

The technical challenge within this idea is probably to make the translation fit with the comment/post, or at least how to make this happen. At this moment we haven’t put further thought into how exactly it should be created, so that’s the challenge. We can either decide the translations should be random or specific to the comment/post that has appeared. 

*For P.S. (Personal Spindoctor)*

The overall idea for this is to make an somewhat nihilistic AI that can/will help the user achieve the best (and possibly the most honest) post to post on a social media platform. We wanted to comment on how many users on social media post their best selves and often not their honest selves. We find that social media accounts are often putting up a façade as to why we acknowledge why some might criticize the lack of “deepness” in some posts on social media.

![ScreenShot](PSflowchart.PNG)

The technical challenges (that we see at the moment) are perhaps making the user-input work and having the AI/spindoctor give a response - along with creating a functional submit-button. In terms of addressing these challenges, we are going look at the p5.js reference list (DOM: input & button), create and use a .JSON file (in terms of possible responses) and style the submit button with CSS.
It would be fun to incorporate the user input in the response somehow, to give a feeling of the response not being entirely random, but this seems to be a last thing to add, as it seems quite complex. How to do it would probably to use this.input somewhere in the response, yet that gives you the entire input which easily can make the response seem broken.

#### What is the value of the individual and the group flowchart that you have produced?

I think creating flowcharts are a good way to search for missing parts or errors. It was much easier to create a flowchart for a project that was already made, than making one for a future project. It's hard to grasp the idea so early on. I think that it will be easier to get further into the making of the program though, now that we have found the basic of our ideas and what we wanna do. But I'm positive that our flowcharts won't exactly end up being vlue prints of our final program. There's something satisfying about creating a flowchart though. 