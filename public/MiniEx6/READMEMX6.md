 ## My Beer Pong Game

### Link to my program:
[MiniEx6 RUNME](https://MiraMorso.gitlab.io/ap-2020/MiniEx6/)

### Link to repository:
https://gitlab.com/MiraMorso/ap-2020/-/tree/master/public/MiniEx6

### Screenshot:
![ScreenShot](Images/screen0.png)
![ScreenShot](Images/screen1.png)

### Questions and Answers:
***Describe how does your game/game objects work?***

My game is about catching the ping pong balls with the red solo cup as a game of virtual beer pong.
When loading the game, a starting screen will appear welcoming you to the game, and you are told to click anywhere to begin. Then you will be taken to the game screen, where you quickly will notice that a red cup is following the cursor on the x axis in the bottom of the screen. A score board is in the left top corner, a bunch of circles are moving around in the background and then a ball will appear from the top in a slow speed. With every ball you catch the speed of the ball will increase to make it harder. 

I have one class object in my program, since I couldn’t make anything else work, without ruining the flow of the game. The class is the many ellipses in the background that are moving around. I added these to make the game harder, and create a sort of distraction to the player. Also to make it easier for myself, since I tried for a long time with no luck to do anything else. 

I had so many problems making and therefore ended up, finding a game that was similar to what I was thinking. I found the "catching game", which I also put in my references in the bottom of this page. After finding that game, I played around with it to figure out what all the elements in the code meant, and then changed everything to match my own idea. I'm happy that I have something to turn in today, but I'm still not totally pleased with the result. I tried making a gif to act as a cooler title for the game, which works fine when you first start the program, but after a GAMEOVER, when you go back to the star screen, then the gif has moved its position to in the top left of the canvas. I tried using translate to make it stay at the same place, but it didn't work, and I have absolutely no idea how to make it work, since I can't seem to find the mistake. 
But it still works, so that is what counts. 



***Describe how you program the objects and their related attributes and methods in your game.***

As said above my class is Bubbles moving around in the background of the game acting as distractions to the player. I made them randomly appear around the `Canvas(600, 400);`. I also made them move random in the space of `random(-5, 5);`, to make them look chaotic. 
In the show function I gave them a white color as stroke, and `noFill();` in an ellipse with both height and width of 50. 
I added for `(var i = 0; i < 100; i++) {` to have 100 ellipses from the beginning. 



***Object-oriented programming***

This way of programming gave me a lot of difficulties, since I made the game first, and therefore it was hard to take any of the elements and turn them into an object. What I did make my object though, worked very well as one. Object-oriented programming is a good way to create an element once that you want to appear multiple times. This class object is built up of abstractions like characteristics, which are how the programmer wants the object to behave and look in the program. 




### References:

"catching game" by wenhuamin
*  https://editor.p5js.org/wenhuamin/sketches/6t6zQ0pNd
