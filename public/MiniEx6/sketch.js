var screen = 0;
var y=-20;
var x=200;
var speed = 3;
var score= 0;

// images
var ball;
var cup;

// my class
var bubble = [];

// preload of my images
function preload() {
  ball = loadImage('Images/ball.png');
  cup = loadImage('Images/cup.png');
  title = loadImage('Images/title.gif')
}

function setup() {
  createCanvas(600, 400);

  // creating <100 bubbles in the background of the canvas
  for (var i = 0; i < 100; i++) {
    let myBubble = new Bubble(i * 100, i * 10, random(-50, 50), random(-50, 50), random(500));
    bubble.push(myBubble);
  }
}

function draw() {
  background(255,180,180)

// making the program run class
  for (var i = 0; i < bubble.length; i++) {
    bubble[i].move();
    bubble[i].show();
  }

//  the three different types of screens
  if(screen == 0){
    startScreen()
  }else if(screen == 1){
    gameOn()
  }else if(screen==2){
    endScreen()
  }
}

// start screen
function startScreen(){
// text in the center of the screen
  push();
  translate(220,50);
  image(title,0,0,150,130);
  pop();
  fill(255)
  textAlign(CENTER);
  textSize(25);
  text('WELCOME TO BEER PONG', width / 2, height / 2)
  text('click anywhere to start', width / 2, height / 2 + 30);
  reset();
}

// game in action
function gameOn(){

// making the cup appear in the bottom of the canvas
  imageMode(CENTER);

// score board in top left corner
  fill(255)
  textSize(25);
  text("score = " + score, 60,30)


  // cup following cursor
  image(cup,mouseX,380,90,90)
  // ping pong balls randomly accuring from top
  image(ball,x,y,20,20)
  y+= speed;
  if(y>height){
    screen =2

  }

// when you get a point the speed will increase with 0.3
  if(y>height-50 && x>mouseX-35 && x<mouseX+35){
    y=-20
    score+= 1
    speed+=.3
  }

// makes the ping pong ball randomly appear with different x values
  if(y==-20){
    pickRandom();
  }
}

function pickRandom(){
  x= random(20,width-20)
}

// Gameover screen when ball hits bottom
function endScreen(){
  background(255,100,100);
  textAlign(CENTER);
  textSize(30);
  fill(255);
  text('GAME OVER', width / 2, height / 2)
  textSize(17)
  text("SCORE = " + score, width / 2, height / 2 + 30)
  text('click anywhere to play again', width / 2, height / 2 + 50);
}

// you go from screen 0 to 1 and from 2 to 0 by pressing mouse
function mousePressed(){
  if(screen==0){
    screen=1

  }else if(screen==2){
    screen=0
      image(title,240,100,150,130)
  }
}

// resets game at gameover
function reset(){
  score=0;
  speed=3;
  y=-20;
}

// class to create a bunch of distraction in the background in form of moving ellipses
class Bubble {
  constructor () {

// they randomly appear all over the canvas
    this.x = random(600);
    this.y = random(400);
  }

// how the bubbles move
  move () {
    this.x = this.x + random(-5, 5);
    this.y = this.y + random(-5, 5);
  }

// bubble's appearance
  show() {
    stroke(255,255,255);
    noFill( );
    ellipse (this.x, this.y, 50, 50);
  }
}
