## Mini Ex1

### Link to my first program:
[Link](https://MiraMorso.gitlab.io/ap-2020/MiniEx1/)

### Screenshot:
![ScreenShot](Skærmbillede 2020-02-07 kl. 12.47.56.png)


### Questions and Answers
**How would you describe your first independent coding experience (in relation to thinking, reading, copying, modifying, writing code, and so on)?**
I had a lot of difficulties as my program wasn't working with my browser at first, so I didn't get to experiment as much as I wanted to. So the final result is not that crazy or anything. But I think that I did get to a point of understanding of the numbers and parameters. I did also try to do some more complicated coding, but I couldn't really make it work, so I kept to the basics, and hopefully will get to the complicated stuff with time.

**How is the coding process different from, or similar to, reading and writing text?**
When you're coding, you're also creating some sort of construction of characters that you as well as someone else are able to understand when reading. It's basically another language just like English. 

**What does code and programming mean to you, and how do the assigned readings help you to reflect on programming?**
I'm confident like the text states that coding will be an essential part of the future if it isn't already, and because of that it's important for people to understand this form of language. With the technology quickly expanding around us, it's important for us to understand what exactly is happening. 
Coding also allows us to be creative and create in a new and non traditional way that I think is very interesting!