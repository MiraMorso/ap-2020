function setup() {
 createCanvas(windowWidth, windowHeight);
// the framrate is determing how quick the trobber is moving
  frameRate(13);
}

function draw() {
// the rectangle is drawn on top of the triangles, and is therefore making them appear with less and less opacity
  fill(70, 80);
  noStroke();
  rect(0, 0, width, height);
  drawElements();
}

function drawElements() {
// there are ten triangles in the throbber
  let num = 10;

push();
// translate is set with mouse X and Y to make the throbber move with the location of the mouse
  translate(mouseX, mouseY);

// makes the triangles and ellipses move in a cirkel of 360 degrees. Every triangle is moved 36 degrees
  let cir = 360/num*(frameCount%num);
  rotate(radians(cir));
  noStroke();

// the colors of the trianlges that together form the trobber are picked at random in the shades of blue and green, and the ellipses are random variations of red and blue
  fill(random(255),10,200);
  ellipse(10,10,10,10,10,);
  fill(10,180,random(255));
  triangle(45,25,35,0,2,15);
pop();

}
