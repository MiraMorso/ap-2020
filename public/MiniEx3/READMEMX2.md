### Link to my program:
[MiniEx3](https://MiraMorso.gitlab.io/ap-2020/MiniEx3/)

### Link to repository: 
https://gitlab.com/MiraMorso/ap-2020/-/tree/master/public/MiniEx3

### Screenshot:
![ScreenShot](ThrobberScreenshot.png)


### Questions and Answers

***Describe your throbber design, both conceptually and technically.***
***What is your sketch? What do you want to explore and/or express?***

I liked the way the throbber, that we were working with in the lesson, was programmed, and decided to play around with that format. I tried to change the different variables in the arguments, and ended up using triangles, which created a form of star looking figure. I then wanted to do something with fill, and make the colors change randomly. So I put the variable determining the blue shades random, and green a higher number than red, to make the colors change in the color scheme of blue and green. I wanted something more to happen, and decided to have ellipses move with the same rotation as the triangles behind them in red and blue color schemes again picked at random. I was still finding the current result of the throbber a bit boring, and wanted to create some form of interaction. I had a conversation with Katrine from the class, and ended up with the idea of making the triangles and ellipses be drawn depending on the location of the mouse, so that the user could be drawing temporarily in a way. I therefore changed my translate syntax to translate(mouseX, mouseY); with the help of Katrine. 
Since being faced with a turning throbber, can be quite frustrating, and feel like a waste of time, I’m happy with the choice of making it more interactive and entertaining. I myself find it very soothing to move the mouse around and see the throbber following along. 
It’s like throbbers force us to take a break with all of our tasks, and it’s kind of terrifying to not have something to do, when we always feel like we’re busy and have a thousand things to do. My goal was to make you forget about how fast you have to constantly be moving, and embrace the little pause that throbbers gives us. 



***What are the time-related syntaxes/functions that you have used in your program, 
and why have you used them in this way? How is time being constructed in computation 
(refer to both the reading materials and your process of coding)? Think about a throbber that you have encountered in digital culture, e.g. for streaming video on YouTube or loading the latest feeds on Facebook, or waiting for a ticket transaction, and consider what a throbber communicates, and/or hides? How might we characterize this icon differently?***

I have used the syntax frameRate, which is the number of frames that is displayed every second, to make it look like the throbber was moving around in circles. I used a low number for the framerate, so it wouldn’t appear as chaotic and quick, but more relaxing, as the sudden break in our task can be so frustrating. We don’t like being interrupted in our doings, and being in a position where we all of a sudden have no control or knowledge of when that state is over, since we are so used to being in full speed in our everyday lives. 
I personally get so awfully impatient, and it won’t be long until I’m either trying to do everything in my power to make the throbber stop, or I’m moving on to another device or task. Being forced to wait for only God knows, how long is an awful feeling. You could be waiting for 20 seconds or 20 minutes that is so frustrating, because how are you going to be planning and managing your time based on that? Impossible. You don’t even have a way of knowing exactly what is causing the throbber to appear. You therefore end up being almost powerless just waiting and getting more and more impatient. But since everything today is technological and high speed, maybe it’s healthy to get a time-out. 
